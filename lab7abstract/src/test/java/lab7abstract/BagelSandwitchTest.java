package lab7abstract;

import static org.junit.Assert.*;

import org.junit.Test;

public class BagelSandwitchTest {
    @Test
    public void addFillingTest()
    {
        BagelSandwich bagelSandwich = new BagelSandwich();

        bagelSandwich.addFilling("Tomato");

        assertEquals("Testing addFilling BagelSandwitch", ", Tomato", bagelSandwich.getFilling());
    }
}
