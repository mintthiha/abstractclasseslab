package lab7abstract;

import static org.junit.Assert.*;

import org.junit.Test;

public class VegetarianSandwitchTest {
    @Test
    public void addFillingTest()
    {
        CeasarSandwich ceasarSandwich = new CeasarSandwich();
        ceasarSandwich.addFilling("Onion");
        assertEquals("Testing addFilling CeaserSandwitch", "Ceaser dressing, Onion", ceasarSandwich.getFilling());
    }

    @Test
    public void isVeganTest()
    {
        CeasarSandwich ceasarSandwich = new CeasarSandwich();
        assertEquals("Testing addFilling CeaserSandwitch", true, ceasarSandwich.isVegan());
    }
}
