package lab7abstract;

public class App {
    public static void main(String[] args) {
        //VegetarianSandwich veganSandwich = new VegetarianSandwich();
        ISandwich sandwich = new TofuSandwich();
        
        if(sandwich instanceof ISandwich){
            System.out.println("yes it's an instance");
        } else {
            System.out.println("No it is not an instance");
        }

        if(sandwich instanceof VegetarianSandwich){
            System.out.println("yes it's an instance");
        } else {
            System.out.println("No it is not an instance");
        }

        //sandwich.addFilling("chicken");

        //sandwich.isVegan();

        ISandwich newSandwich = (VegetarianSandwich) sandwich;
        //newSandwich.isVegan();

        VegetarianSandwich ceasarSandwich = new CeasarSandwich();
        System.out.println(ceasarSandwich.isVegetarian());

    }
}
