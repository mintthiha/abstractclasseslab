package lab7abstract;

public class TofuSandwich extends VegetarianSandwich{

    public TofuSandwich(){
        super();
        this.filling = "Tofu";
    }

    @Override
    public String getProtein(){
        return "Tofu";
    }
}
