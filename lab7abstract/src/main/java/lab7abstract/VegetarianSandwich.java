package lab7abstract;

public abstract class VegetarianSandwich implements ISandwich{
    protected String filling;

    public VegetarianSandwich(){
        this.filling = "";
    }

    public void addFilling(String topping){
        String[] meats = {"chicken", "beef", "fish", "meat", "pork"};
        for (int i = 0; i < meats.length; i++) {
            if(topping.equals(meats[i])){
                throw new IllegalArgumentException("The topping can't be meat.");
            }
        }
        this.filling += ", " + topping;
    }

    public String getFilling(){
        return this.filling;
    }

    public boolean isVegetarian(){ //Adding final here makes the program uncompilable. (Step 5, last question)
        return true;
    }

    public boolean isVegan(){
        boolean isVegan = true;
        String[] meats = {"egg", "cheese"};
        for (int i = 0; i < meats.length; i++) {
            if(this.filling.equals(meats[i])){
                isVegan = false;
            }
        }
        return isVegan;
    }

    public abstract String getProtein(); //dont unit test this
}
