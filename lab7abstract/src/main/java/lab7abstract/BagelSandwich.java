package lab7abstract;

public class BagelSandwich implements ISandwich{
    private String filling;

    public BagelSandwich(){
        this.filling = "";
    }

    public String getFilling(){
        return this.filling;
    }

    public void addFilling(String topping){
        this.filling += ", " + topping;
    }

    public boolean isVegetarian(){
        throw new UnsupportedOperationException("This is not a vegan sandwich.");
    }
}
