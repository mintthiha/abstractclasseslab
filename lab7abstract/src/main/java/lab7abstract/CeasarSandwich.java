package lab7abstract;

public class CeasarSandwich extends VegetarianSandwich {

    public CeasarSandwich(){
        super();
        this.filling = "Ceaser dressing";
        
    }

    @Override
    public String getProtein(){
        return "Anchovies";
    }

    @Override
    public boolean isVegetarian(){ 
        return false;
    }
}
